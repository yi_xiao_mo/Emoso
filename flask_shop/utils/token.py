'''
1.加密的数据
    userid
2.加密的算法
    pip install pyjwt
3.加密的密钥
    SECRET_KEY
'''
import time
from functools import wraps

from flask import request
import jwt

from flask import current_app


#生成token
def generate_token(data):
    '''
    data:要加密的数据类型，数据类型是字典
    '''
    #设置数据的过期时间
    data.update({'exp':time.time()+current_app.config['JWT_EXPIRATION_DATA']})   #一小时过期
    #数据的加密
    token = jwt.encode(data,current_app.config['SECRET_KEY'],algorithm="HS256")
    return token

#解密token
def verif_token(token): 
    #数据的解密
    try:
        data = jwt.decode(token,current_app.config['SECRET_KEY'],algorithm=["HS256"])
    except Exception as e:
        return None
    return data

def login_required(view_func):
    @wraps(view_func)
    def verif_token_info(*args,**kwargs):
        #获取用户传递来的token
        token = request.headers.get('token')
        #解析 token
        if verif_token(token):
            return view_func(*args,**kwargs)
        else:
            return{'status':400,'msg':'token 无效'}
        #返回函数
    return verif_token_info